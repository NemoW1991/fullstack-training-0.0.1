# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Container Scanning customization: https://docs.gitlab.com/ee/user/application_security/container_scanning/#customizing-the-container-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence
image:
  name: gcr.io/kaniko-project/executor:debug
  entrypoint: [""]

variables:
  #More Information on Kaniko Caching: https://cloud.google.com/build/docs/kaniko-cache
  KANIKO_CACHE_ARGS: "--cache=true --cache-copy-layers=true --cache-ttl=24h"
  VERSIONLABELMETHOD: "OnlyIfThisCommitHasVersion" # options: "OnlyIfThisCommitHasVersion","LastVersionTagInGit"
  IMAGE_LABELS: >
    --label org.opencontainers.image.vendor=$CI_SERVER_URL/$GITLAB_USER_LOGIN
    --label org.opencontainers.image.authors=$CI_SERVER_URL/$GITLAB_USER_LOGIN
    --label org.opencontainers.image.revision=$CI_COMMIT_SHA
    --label org.opencontainers.image.source=$CI_PROJECT_URL
    --label org.opencontainers.image.documentation=$CI_PROJECT_URL
    --label org.opencontainers.image.licenses=$CI_PROJECT_URL
    --label org.opencontainers.image.url=$CI_PROJECT_URL
    --label vcs-url=$CI_PROJECT_URL
    --label com.gitlab.ci.user=$CI_SERVER_URL/$GITLAB_USER_LOGIN
    --label com.gitlab.ci.email=$GITLAB_USER_EMAIL
    --label com.gitlab.ci.tagorbranch=$CI_COMMIT_REF_NAME
    --label com.gitlab.ci.pipelineurl=$CI_PIPELINE_URL
    --label com.gitlab.ci.commiturl=$CI_PROJECT_URL/commit/$CI_COMMIT_SHA
    --label com.gitlab.ci.cijoburl=$CI_JOB_URL
    --label com.gitlab.ci.mrurl=$CI_PROJECT_URL/-/merge_requests/$CI_MERGE_REQUEST_ID

get-latest-git-version:
  stage: .pre
  image:
    name: alpine/git
    entrypoint: [""]
  rules:
    - if: '$VERSIONLABELMETHOD == "LastVersionTagInGit"'
  script:
    - |
      echo "the google kaniko container does not have git and does not have a package manager to install it"
      git clone https://github.com/GoogleContainerTools/kaniko.git
      cd kaniko
      echo "$(git describe --abbrev=0 --tags)" > ../VERSIONTAG.txt
      echo "VERSIONTAG.txt contains $(cat ../VERSIONTAG.txt)"
  artifacts:
    paths:
      - VERSIONTAG.txt

sast:
  stage: test
include:
- template: Security/SAST.gitlab-ci.yml

.build_with_kaniko:
  #Hidden job to use as an "extends" template
  stage: build
  script:
    - |
      echo "Building and shipping image to $CI_REGISTRY_IMAGE"
      #Build date for opencontainers
      BUILDDATE="'$(date '+%FT%T%z' | sed -E -n 's/(\+[0-9]{2})([0-9]{2})$/\1:\2/p')'" #rfc 3339 date
      IMAGE_LABELS="$IMAGE_LABELS --label org.opencontainers.image.created=$BUILDDATE --label build-date=$BUILDDATE"
      #Description for opencontainers
      BUILDTITLE=$(echo $CI_PROJECT_TITLE | tr " " "_")
      IMAGE_LABELS="$IMAGE_LABELS --label org.opencontainers.image.title=$BUILDTITLE --label org.opencontainers.image.description=$BUILDTITLE"
      #Add ref.name for opencontainers
      IMAGE_LABELS="$IMAGE_LABELS --label org.opencontainers.image.ref.name=$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME"

      #Build Version Label and Tag from git tag, LastVersionTagInGit was placed by a previous job artifact
      if [[ "$VERSIONLABELMETHOD" == "LastVersionTagInGit" ]]; then VERSIONLABEL=$(cat VERSIONTAG.txt); fi
      if [[ "$VERSIONLABELMETHOD" == "OnlyIfThisCommitHasVersion" ]]; then VERSIONLABEL=$CI_COMMIT_TAG; fi
      if [[ ! -z "$VERSIONLABEL" ]]; then 
        IMAGE_LABELS="$IMAGE_LABELS --label org.opencontainers.image.version=$VERSIONLABEL"
        ADDITIONALTAGLIST="$ADDITIONALTAGLIST $VERSIONLABEL"
      fi
      
      ADDITIONALTAGLIST="$ADDITIONALTAGLIST $CI_COMMIT_REF_NAME $CI_COMMIT_SHORT_SHA"
      if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then ADDITIONALTAGLIST="$ADDITIONALTAGLIST latest"; fi
      if [[ -n "$ADDITIONALTAGLIST" ]]; then 
        for TAG in $ADDITIONALTAGLIST; do 
          FORMATTEDTAGLIST="${FORMATTEDTAGLIST} --tag $CI_REGISTRY_IMAGE:$TAG "; 
        done; 
      fi
      
      #Reformat Docker tags to kaniko's --destination argument:
      FORMATTEDTAGLIST=$(echo "${FORMATTEDTAGLIST}" | sed s/\-\-tag/\-\-destination/g) 

      echo "Kaniko arguments to run: --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/dockerfile $KANIKO_CACHE_ARGS $FORMATTEDTAGLIST $IMAGE_LABELS"
      mkdir -p /kaniko/.docker
      echo "{\"auths\":{\"$CI_REGISTRY\":{\"auth\":\"$(echo -n $CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD | base64)\"}}}" > /kaniko/.docker/config.json
      /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/dockerfile $KANIKO_CACHE_ARGS $FORMATTEDTAGLIST $IMAGE_LABELS


build-for-gitlab-project-registry:
  extends: .build_with_kaniko
  environment:
    #This is only here for completeness, since there are no CI CD Variables with this scope, the project defaults are used
    # to push to this projects docker registry
    name: push-to-gitlab-project-registry

deploy:
  image:
    name: bitnami/kubectl:latest
    entrypoint: ['']
  script:
    - kubectl config get-contexts
    - kubectl config use-context optus-web/fullstack-training-0.0.1:default
    - |
      kubectl apply -f - <<EOF
      apiVersion: apps/v1
      kind: Deployment
      metadata:
        name: $CI_COMMIT_REF_NAME-service
      spec:
        replicas: 1
        selector:
          matchLabels:
            app: $CI_COMMIT_REF_NAME-service
        template:
          metadata:
            labels:
              app: $CI_COMMIT_REF_NAME-service
          spec:
            containers:
            - name: $CI_COMMIT_REF_NAME-service
              image: registry.gitlab.com/optus-web/fullstack-training-0.0.1:$CI_COMMIT_SHORT_SHA
              ports:
              - containerPort: 80
      EOF
    - |
      kubectl apply -f - <<EOF
      apiVersion: v1
      kind: Service
      metadata:
        name: $CI_COMMIT_REF_NAME-service
      spec:
        ports:
        - port: 80
          targetPort: 3001
        selector:
          app: $CI_COMMIT_REF_NAME-service
        type: ClusterIP
      EOF
    - |
      kubectl apply -f - <<EOF
      apiVersion: networking.k8s.io/v1
      kind: Ingress
      metadata:
        name: $CI_COMMIT_REF_NAME-service-ingress
        annotations:
          cert-manager.io/cluster-issuer: letsencrypt
      spec:
        tls:
        - hosts:
          - $CI_COMMIT_REF_NAME-service.oca-web.com
          secretName: $CI_COMMIT_REF_NAME-service-ingress-tls
        rules:
        - host: $CI_COMMIT_REF_NAME-service.oca-web.com
          http:
            paths:
            - backend:
                service:
                  name: $CI_COMMIT_REF_NAME-service
                  port:
                    number: 80
              path: /
              pathType: Prefix
      EOF
    - echo "🌿🐨🌿🐨🌿 Deployed to $CI_COMMIT_REF_NAME-service.oca-web.com"